QT += core gui network

lessThan(QT_MAJOR_VERSION, 5) {
    DEFINES += IS_QT4
    packagesExist(phonon) {
        QT += phonon
        DEFINES += USE_PHONON
    }
    unix:!macx {
        LIBS += -lX11 -lXfixes
    }
} else {
    QT += widgets
    packagesExist(phonon4qt5) {
        QT += phonon4qt5
        DEFINES += USE_PHONON
    }
}

TARGET = poniesbox
DEFINES += NAME=\\\"$${TARGET}\\\"
OBJECTS_DIR = obj
MOC_DIR = src/moc
UI_DIR = src/ui
RCC_DIR = src/rcc
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11 -Wextra

SOURCES += src/main.cpp \
    src/pony.cpp \
    src/behavior.cpp \
    src/effect.cpp \
    src/speak.cpp \
    src/configwindow.cpp \
    src/csv_parser.cpp \
    src/interaction.cpp \
    src/singleapplication.cpp

HEADERS  += src/pony.h \
    src/behavior.h \
    src/effect.h \
    src/speak.h \
    src/configwindow.h \
    src/csv_parser.h \
    src/interaction.h \
    src/singleapplication.h

FORMS += src/configwindow.ui

TRANSLATIONS = translations/$${TARGET}_de.ts \
    translations/$${TARGET}_ru.ts \
    translations/$${TARGET}_uk.ts \
    translations/$${TARGET}_pl.ts \
    translations/$${TARGET}_fr.ts \
    translations/$${TARGET}_el.ts

RESOURCES += $${TARGET}.qrc

unix {
    isEmpty(PREFIX) {
        PREFIX = /usr
    }

    BINDIR = $${PREFIX}/bin
    DATADIR =$${PREFIX}/share
    DEFINES += DATA_PATH=\\\"$${DATADIR}/$${TARGET}\\\"

    target.path = $${BINDIR}/

    data.path = $${DATADIR}/$${TARGET}/
    data.files = desktop-ponies

    translations.path = $${DATADIR}/$${TARGET}/translations/
    translations.files = translations/*.qm

    icon.path = $${DATADIR}/icons/hicolor/
    icon.files = res/icons/*

    desktop.path = $${DATADIR}/applications/
    desktop.files = res/$${TARGET}.desktop

    INSTALLS += target data translations icon desktop
} else {
    DEFINES += DATA_PATH=\\\".\\\"
}
