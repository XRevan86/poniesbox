<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl_PL">
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/configwindow.ui" line="14"/>
        <source>Poniesbox</source>
        <translation>Poniesbox</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="63"/>
        <source>Add pony</source>
        <translation>Dodaj kucyka</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="220"/>
        <source>Remove pony</source>
        <translation>Usuń kucyka</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="293"/>
        <source>&amp;Main</source>
        <translation>&amp;Główny</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="309"/>
        <source>Show the ponies in half size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="312"/>
        <source>&amp;Small ponies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="345"/>
        <source>Toggles if ponies are always on top of other windows</source>
        <translation>Przełącza czy kucyki mają być zawsze na wierzchu</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="348"/>
        <source>&amp;Always on top</source>
        <translation>Z&amp;awsze na wierzchu</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="472"/>
        <source>Hide &amp;tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="322"/>
        <source>Bypass the X11 window manager, showing ponies on every desktop on top of every window</source>
        <translation>Omija manager okien, pokazując kucyki na wierzchu na każdym pulpicie</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="325"/>
        <source>Show on every &amp;virtual desktop</source>
        <translation>Pokaż na każdym pulpicie &amp;wirtualnym</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="388"/>
        <source>Specifies in which directory the pony data and configuration are</source>
        <translation>Określa w jakim katalogu znajdują się dane kucyków</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="391"/>
        <source>Pony data di&amp;rectory</source>
        <translation>Katalog danych k&amp;ucyków</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="358"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="365"/>
        <source>Toggle if interactions are to be executed</source>
        <translation>Włącza interakcje między kucykami</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="368"/>
        <source>&amp;Interactions enabled</source>
        <translation>&amp;Interakcje włączone</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="411"/>
        <source>Toggle if effects are enabled</source>
        <translation>Włącza efekty dla zachowań kucyków</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="414"/>
        <source>Ef&amp;fects enabled</source>
        <translation>E&amp;fekty włączone</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="490"/>
        <source>S&amp;peech</source>
        <translation>&amp;Mowa</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="508"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="537"/>
        <source>For how long the speech is to stay on screen</source>
        <translation>Jak długo wiadomości tekstowe mają pozostawać na ekranie</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="540"/>
        <source>&amp;Text delay</source>
        <translation>&amp;Czas wyświetlania wiadomości</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="550"/>
        <source>Toggles if ponies are to speak</source>
        <translation>Włącza mowę kucyków</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="553"/>
        <source>&amp;Speech enabled</source>
        <translation>Mow&amp;a włączona</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="582"/>
        <source>NOT ACTIVE. Toggles the playing of sounds when a pony speaks.</source>
        <translation>NIEAKTYWNE. Przełącza odtwarzanie dźwięków gdy kucyk mówi.</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="585"/>
        <source>Play s&amp;ounds</source>
        <translation>Odtwarzaj dźwię&amp;ki</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="605"/>
        <source>How frequently ponies speak</source>
        <translation>Jak często kucyk mówi</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="608"/>
        <source>Sp&amp;eech probability</source>
        <translation>Pra&amp;wdopodobieństwo mowy</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="621"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="262"/>
        <source>Accept</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="282"/>
        <source>Reset</source>
        <translation>Zresetuj</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="77"/>
        <source>Open configuration</source>
        <translation>Otwórz konfigurację</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="78"/>
        <source>Close application</source>
        <translation>Zamknij aplikację</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="29"/>
        <source>Add ponies</source>
        <translation>Dodaj kucyki</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="205"/>
        <source>Active ponies</source>
        <translation>Aktywne kucyki</translation>
    </message>
    <message>
        <location filename="../src/configwindow.ui" line="256"/>
        <source>Configuration</source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <location filename="../src/configwindow.cpp" line="425"/>
        <source>Select pony data directory</source>
        <translation>Wybierz katalog danych kucyków</translation>
    </message>
</context>
<context>
    <name>Pony</name>
    <message>
        <location filename="../src/pony.cpp" line="163"/>
        <source>Sleeping</source>
        <translation>Śpi</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="170"/>
        <source>Remove %1</source>
        <translation>Usuń %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="171"/>
        <source>Remove every %1</source>
        <translation>Usuń każdego %1</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="172"/>
        <source>Open configuration</source>
        <translation>Otwórz konfigurację</translation>
    </message>
    <message>
        <location filename="../src/pony.cpp" line="174"/>
        <source>Close application</source>
        <translation>Zamknij aplikację</translation>
    </message>
</context>
</TS>
